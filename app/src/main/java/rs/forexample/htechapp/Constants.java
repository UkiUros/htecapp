package rs.forexample.htechapp;

/**
 * Created by urossimic on 8/21/16.
 */

public class Constants {

    public static final String TOPICS_URL = "https://raw.githubusercontent.com/danieloskarsson/mobile-coding-exercise/master/items.json";


    public static final String EXTRA_TOPIC = "extra_topic";

    public static final String TITLE_TAG = "title";
    public static final String DESCRIPTION_TAG = "description";
    public static final String IMAGE_TAG = "image";
}
