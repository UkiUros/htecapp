package rs.forexample.htechapp.activities;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import rs.forexample.htechapp.BaseActivity;
import rs.forexample.htechapp.Constants;
import rs.forexample.htechapp.R;
import rs.forexample.htechapp.data.model.Topic;
import rs.forexample.htechapp.utilities.ResUtils;

public class DetailedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        showBackArrow(true);
        setTitle(R.string.title_preview);

        if (!getIntent().hasExtra(Constants.EXTRA_TOPIC)){
            finish();
            return;
        }

        Topic mTopic = getIntent().getParcelableExtra(Constants.EXTRA_TOPIC);

        TextView tvTitle = (TextView) findViewById(R.id.title);
        TextView tvDescription = (TextView) findViewById(R.id.description);
        ImageView imageView = (ImageView) findViewById(R.id.image);

        tvTitle.setText(mTopic.getTitle());
        tvDescription.setText(mTopic.getDescription());

        Picasso.with(this)
                .load(mTopic.getImageUrl())
                .placeholder(new ColorDrawable(ResUtils.getResColor(this, R.color.img_placeholder)))
                .fit()
                .centerInside()
                .tag(this)
                .into(imageView);

    }
}
