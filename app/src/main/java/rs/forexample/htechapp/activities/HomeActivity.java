package rs.forexample.htechapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;

import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import rs.forexample.htechapp.BaseActivity;
import rs.forexample.htechapp.Constants;
import rs.forexample.htechapp.R;
import rs.forexample.htechapp.adapters.TopicRecyclerAdapter;
import rs.forexample.htechapp.api.ServiceApi;
import rs.forexample.htechapp.customs.SimpleDividerItemDecoration;
import rs.forexample.htechapp.data.model.Topic;

public class HomeActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private View noDataLayout;

    private TopicRecyclerAdapter adapter;
    private List<Topic> mTopicList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        showBackArrow(false);
        setTitle(R.string.app_name);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        noDataLayout = findViewById(R.id.noData);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        registerForContextMenu(recyclerView);

        mTopicList = new ArrayList<>();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                onRefresh();
            }
        });
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        ServiceApi.getTopics(new ServiceApi.Listener<List<Topic>>() {
            @Override
            public void onResponse(List<Topic> response) {
                onDataLoaded(response);
            }

            @Override
            public void onError(VolleyError error) {
                onDataLoaded(null);
            }
        });
    }

    private void onDataLoaded(final List<Topic> topicList){
        swipeRefreshLayout.setRefreshing(false);
        if (topicList == null || topicList.size() < 1) {
            recyclerView.setVisibility(View.GONE);
            noDataLayout.setVisibility(View.VISIBLE);
            return;
        }

        recyclerView.setVisibility(View.VISIBLE);
        noDataLayout.setVisibility(View.GONE);

        if (mTopicList != null) mTopicList.clear();

        mTopicList.addAll(topicList);

        if (adapter == null){
            AnimationSet set = new AnimationSet(true);

            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(500);
            set.addAnimation(animation);

            animation = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
            );
            animation.setDuration(100);
            set.addAnimation(animation);

            LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);

            adapter = new TopicRecyclerAdapter(this, mTopicList);
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(
                    getApplicationContext()
            ));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setLayoutAnimation(controller);
            recyclerView.setAdapter(adapter);

            adapter.setOnItemClickListener(new TopicRecyclerAdapter.OnItemClickListener() {
                @Override
                public void itemClicked(View view, int position) {
                    Topic topic = topicList.get(position);

                    startActivity(new Intent(HomeActivity.this, DetailedActivity.class)
                            .putExtra(Constants.EXTRA_TOPIC, topic));
                }
            });

        }else{
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {}
}
