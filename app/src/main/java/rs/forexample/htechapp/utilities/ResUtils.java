package rs.forexample.htechapp.utilities;

import android.content.Context;
import android.os.Build;

/**
 * Created by urossimic on 8/21/16.
 */

public class ResUtils {

    public static boolean sdkBeforeM() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M;
    }

    public static int getResColor(Context context, int resId) {
        return sdkBeforeM() ? context.getResources().getColor(resId) : context.getColor(resId);
    }
}
