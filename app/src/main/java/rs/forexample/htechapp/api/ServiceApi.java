package rs.forexample.htechapp.api;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.List;

import rs.forexample.htechapp.HtechApplication;
import rs.forexample.htechapp.data.JsonParser;
import rs.forexample.htechapp.data.model.Topic;

import static rs.forexample.htechapp.Constants.TOPICS_URL;

/**
 * Created by urossimic on 8/21/16.
 */

public class ServiceApi {

    public interface Listener<T> {
        void onResponse(T response);

        void onError(VolleyError error);
    }

    public static void getTopics(final Listener<List<Topic>> responseListener) {
        JsonArrayRequest request = new JsonArrayRequest(TOPICS_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                responseListener.onResponse(JsonParser.parseTopics(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseListener.onError(error);
            }
        });

        HtechApplication.getInstance().addToRequestQueue(request);

    }
}
