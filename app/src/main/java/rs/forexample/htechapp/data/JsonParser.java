package rs.forexample.htechapp.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rs.forexample.htechapp.Constants;
import rs.forexample.htechapp.data.model.Topic;

/**
 * Created by urossimic on 8/21/16.
 */

public class JsonParser {

    public static List<Topic> parseTopics(JSONArray rawJson) {

        List<Topic> topicList = new ArrayList<>();

        try {

            for (int i = 0; i < rawJson.length(); i++) {
                topicList.add(parseSingleTopic(rawJson.getJSONObject(i)));
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return topicList;
    }

    private static Topic parseSingleTopic(JSONObject rawJson) {

        try {

            String title = rawJson.getString(Constants.TITLE_TAG);
            String description = rawJson.getString(Constants.DESCRIPTION_TAG);
            String imageUrl = rawJson.getString(Constants.IMAGE_TAG);

            return new Topic(title, description, imageUrl);

        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }

    }
}
