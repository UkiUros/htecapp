package rs.forexample.htechapp.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import rs.forexample.htechapp.R;
import rs.forexample.htechapp.data.model.Topic;
import rs.forexample.htechapp.utilities.ResUtils;

/**
 * Created by urossimic on 8/21/16.
 */

public class TopicRecyclerAdapter extends RecyclerView.Adapter<TopicRecyclerAdapter.MyViewHolder>{

    private List<Topic> topics;
    private Context ctx;

    private OnItemClickListener onItemClickListener;

    public TopicRecyclerAdapter(Context ctx, List<Topic> topics) {
        this.topics = topics;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_topic, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Topic t = topics.get(position);

        Picasso.with(ctx)
                .load(t.getImageUrl())
                .placeholder(new ColorDrawable(ResUtils.getResColor(ctx, R.color.img_placeholder)))
                .fit()
                .centerInside()
                .tag(ctx)
                .into(holder.imageView);

        holder.tvTitle.setText(t.getTitle());
        holder.tvDescription.setText(t.getDescription());

        holder.viewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) onItemClickListener.itemClicked(v, holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    /*
        Teo just scored 3 points vs Team USA
     */

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvDescription;
        public ImageView imageView;
        public View viewContainer;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.title);
            tvDescription = (TextView) itemView.findViewById(R.id.description);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            viewContainer = itemView.findViewById(R.id.itemContainer);
        }
    }

    public interface OnItemClickListener {
        void itemClicked(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}
